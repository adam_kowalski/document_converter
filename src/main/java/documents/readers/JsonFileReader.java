package documents.readers;

import documents.exceptions.FileReaderException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class JsonFileReader implements IFileReader {
    private static final String FILE_DEFAULT_ENCODING = "UTF-8";

    @Override
    public List<Map<String, String>> read(String filePath) throws FileReaderException {
        List<Map<String, String>> result = new ArrayList<>();
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(filePath));
            String fileContent = new String(encoded, FILE_DEFAULT_ENCODING);
            JSONArray jsonFromFile = new JSONArray(fileContent);

            for (int i = 0; i < jsonFromFile.length(); i++) {
                JSONObject jsonObject = new JSONObject(jsonFromFile.get(i).toString());
                Map<String, String> record = new LinkedHashMap<>();
                for (String key : jsonObject.keySet()) {
                    record.put(key, jsonObject.get(key).toString());
                }
                result.add(record);
            }

        } catch (IOException e) {
            throw new FileReaderException(e.getMessage(), e.getCause());

        }
        return result;
    }
}
