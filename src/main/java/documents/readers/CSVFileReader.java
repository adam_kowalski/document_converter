package documents.readers;

import documents.exceptions.FileReaderException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class CSVFileReader implements IFileReader {

    private static final String CSV_FILE_SEPARATOR = ";";

    @Override
    public List<Map<String, String>> read(String filePath) throws FileReaderException {

        List<Map<String, String>> rulesMap = new ArrayList<>();
        try {
            File file = new File(filePath);
            Scanner scanner = new Scanner(file);
            String[] headLines = scanner.nextLine().split(CSV_FILE_SEPARATOR);

            String line;
            while (scanner.hasNextLine() && !(line = scanner.nextLine()).isEmpty()) {
                String[] rule = line.split(CSV_FILE_SEPARATOR);
                Map<String, String> rules = new LinkedHashMap<>();

                for (int i = 0; i < headLines.length; i++) {
                    rules.put(headLines[i], rule[i]);
                }
                rulesMap.add(rules);
            }

        } catch (FileNotFoundException e) {
            throw new FileReaderException(e.getMessage(), e.getCause());
        }
        return rulesMap;
    }
}
