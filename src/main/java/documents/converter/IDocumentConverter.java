package documents.converter;

import documents.exceptions.FileReaderException;
import documents.exceptions.FileWriterException;

public interface IDocumentConverter {
    public void convert(String inputFilePath, String outputFilePath) throws FileReaderException, FileWriterException;
}
