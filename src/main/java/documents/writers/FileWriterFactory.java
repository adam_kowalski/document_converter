package documents.writers;

public class FileWriterFactory {
    public IFileWriter produce(String filePath) {
        IFileWriter result = null;

        if (filePath.endsWith(".csv")) {
            result = new CSVFileWriter();
        }

        if (filePath.endsWith(".json")) {
            result = new JsonFileWriter();
        }

        return result;
    }
}
